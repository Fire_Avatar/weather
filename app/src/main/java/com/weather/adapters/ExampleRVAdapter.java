package com.weather.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.weather.R;
import com.weather.models.WeatherModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExampleRVAdapter extends RecyclerView.Adapter<ExampleRVAdapter.ViewHolder> {

    protected List<WeatherModel.Data.Weather> mWeathers = new ArrayList<>();
    private Context context;
    private OnItemClickListener onItemClickListener;

    public ExampleRVAdapter(List<WeatherModel.Data.Weather> weathers, Context context) {
        mWeathers = weathers;
        this.context = context;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.simple_weather_item, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        String url = mWeathers.get(position).hourly.get(0).weatherIconUrl.get(0).value;
        String celsius = context.getString(R.string.celsius);
        String temp = mWeathers.get(position).hourly.get(0).tempC + celsius;
        String feelTemp = mWeathers.get(position).hourly.get(0).feelsLikeC + celsius;
        Picasso.with(context)
                .load(url)
                .resize(254, 254)
                .into(holder.weatherIcon);
        holder.mDateTextView.setText(mWeathers.get(position).date);
        holder.mDescTextView.setText(mWeathers.get(position).hourly.get(0).weatherDesc.get(0).value);
        holder.mTemperatureTextView.setText(temp);
        holder.mFeelTemperatureTextView.setText(feelTemp);
        holder.mPressure.setText(mWeathers.get(position).hourly.get(0).pressure);

    }

    @Override
    public int getItemCount() {
        return mWeathers.size();
    }

    public interface OnItemClickListener {
        void onItemClick(WeatherModel item, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @BindView(R.id.weather_icon)
        ImageView weatherIcon;
        @BindView(R.id.date)
        TextView mDateTextView;
        @BindView(R.id.description)
        TextView mDescTextView;
        @BindView(R.id.temperature)
        TextView mTemperatureTextView;
        @BindView(R.id.feel_temperature)
        TextView mFeelTemperatureTextView;
        @BindView(R.id.pressure)
        TextView mPressure;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(null, getAdapterPosition());
            } else {
                throw new RuntimeException("You must init OnItemClickListener before call method "
                        + "on itemClick()!");
            }
        }


    }
}
