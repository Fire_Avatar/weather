package com.weather.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class WeatherModel implements Serializable {
    public Data data;

    @Override
    public String toString() {
        return "WeatherModel{" +
                "data=" + data +
                '}';
    }

    public static class Data implements Serializable {
        public ArrayList<Request> request;
        @SerializedName("current_condition")
        public ArrayList<CurrentCondition> currentCondition;
        public ArrayList<Weather> weather;

        @Override
        public String toString() {
            return "Data{" +
                    "request=" + request.toString() +
                    ", currentCondition=" + currentCondition.toString() +
                    ", weather=" + weather.toString() +
                    '}';
        }

        public static class Request implements Serializable {
            public String type;
            public String query;

            @Override
            public String toString() {
                return "Request{" +
                        "type='" + type + '\'' +
                        ", query='" + query + '\'' +
                        '}';
            }
        }

        public static class CurrentCondition implements Serializable {
            @SerializedName("temp_C")
            public String tempC;
            @SerializedName("temp_F")
            public String tempF;
            public ArrayList<WeatherIconUrl> weatherIconUrl;
            public ArrayList<WeatherIconDesc> weatherDesc;
            @SerializedName("windspeedKmph")
            public String windSpeedKmph;
            public String humidity;
            public String visibility;
            public String pressure;
            @SerializedName("FeelsLikeC")
            public String feelsLikeC;
            @SerializedName("FeelsLikeF")
            public String feelsLikeF;

            @Override
            public String toString() {
                return "CurrentCondition{" +
                        "tempC='" + tempC + '\'' +
                        ", tempF='" + tempF + '\'' +
                        ", weatherIconUrl=" + weatherIconUrl.toString() +
                        ", weatherIconDescs=" +weatherDesc.toString() +
                        ", windSpeedKmph='" + windSpeedKmph + '\'' +
                        ", humidity='" + humidity + '\'' +
                        ", visibility='" + visibility + '\'' +
                        ", pressure='" + pressure + '\'' +
                        ", feelsLikeC='" + feelsLikeC + '\'' +
                        ", feelsLikeF='" + feelsLikeF + '\'' +
                        '}';
            }

            public static class WeatherIconUrl implements Serializable {
                public String value;

                @Override
                public String toString() {
                    return "WeatherIconUrl{" +
                            "value='" + value + '\'' +
                            '}';
                }
            }

            public static class WeatherIconDesc implements Serializable {
                public String value;

                @Override
                public String toString() {
                    return "WeatherIconDesc{" +
                            "value='" + value + '\'' +
                            '}';
                }
            }
        }

        public static class Weather implements Serializable {
            public String date;
            public ArrayList<Astronomy> astronomy;
            public ArrayList<Hourly> hourly;

            @Override
            public String toString() {
                return "Weather{" +
                        "date='" + date + '\'' +
                        ", astronomies=" + astronomy.toString() +
                        ", hourlies=" + hourly.toString() +
                        '}';
            }

            public static class Astronomy implements Serializable {
                public String sunrise;
                public String sunset;

                @Override
                public String toString() {
                    return "Astronomy{" +
                            "sunrise='" + sunrise + '\'' +
                            ", sunset='" + sunset + '\'' +
                            '}';
                }
            }

            public static class Hourly implements Serializable {
                public String tempC;
                public String tempF;
                public ArrayList<WeatherIconUrl> weatherIconUrl;
                public ArrayList<WeatherIconDesc> weatherDesc;
                @SerializedName("windspeedKmph")
                public String windSpeedKmph;
                public String humidity;
                public String visibility;
                public String pressure;
                @SerializedName("FeelsLikeC")
                public String feelsLikeC;
                @SerializedName("FeelsLikeF")
                public String feelsLikeF;
                @SerializedName("cloudcover")
                public String cloudCover;

                @Override
                public String toString() {
                    return "Hourly{" +
                            "tempC='" + tempC + '\'' +
                            ", tempF='" + tempF + '\'' +
                            ", weatherIconUrl=" + weatherIconUrl.toString() +
                            ", weatherIconDescs=" + weatherDesc.toString() +
                            ", windSpeedKmph='" + windSpeedKmph + '\'' +
                            ", humidity='" + humidity + '\'' +
                            ", visibility='" + visibility + '\'' +
                            ", pressure='" + pressure + '\'' +
                            ", feelsLikeC='" + feelsLikeC + '\'' +
                            ", feelsLikeF='" + feelsLikeF + '\'' +
                            ", cloudCover='" + cloudCover + '\'' +
                            '}';
                }

                public static class WeatherIconUrl implements Serializable {
                    public String value;

                    @Override
                    public String toString() {
                        return "WeatherIconUrl{" +
                                "value='" + value + '\'' +
                                '}';
                    }
                }

                public static class WeatherIconDesc implements Serializable {
                    public String value;

                    @Override
                    public String toString() {
                        return "WeatherIconDesc{" +
                                "value='" + value + '\'' +
                                '}';
                    }
                }
            }
        }
    }
}
