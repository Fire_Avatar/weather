package com.weather.database;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.weather.models.WeatherModel;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;


public class DataKeeper {

    private Context context;

    public DataKeeper(Context context) {
        this.context = context;
    }

    public void saveWeather(WeatherModel weatherModel) {
        FileOutputStream fos = null;

        try {
            fos = context.openFileOutput("weather_data", Context.MODE_PRIVATE);

            fos.write(new Gson().toJson(weatherModel, WeatherModel.class).getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public WeatherModel getWeather() {
        WeatherModel weatherModel = null;
        FileInputStream fis = null;

        try {
            fis = context.openFileInput("weather_data");

            BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
            StringBuilder dataBuilder = new StringBuilder();
            String data;

            while ((data = reader.readLine()) != null) {
                dataBuilder.append(data);
            }

            weatherModel = new Gson().fromJson(dataBuilder.toString(), WeatherModel.class);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return weatherModel;
    }

    public boolean isWeatherDataExist() {
        boolean isExist = false;

        FileInputStream fis = null;

        try {
            fis = context.openFileInput("weather_data");

            BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
            StringBuilder dataBuilder = new StringBuilder();
            String data;
 
            while ((data = reader.readLine()) != null) {
                dataBuilder.append(data);
            }

            if (!dataBuilder.toString().isEmpty()) {
                isExist = true;
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return isExist;
    }

    public boolean isTimeToUpdate() {
        boolean isTimeUpdate = false;

        SharedPreferences sharedPreferences = context.getSharedPreferences("time shared preferences"
                , Context.MODE_PRIVATE);

        if (System.currentTimeMillis() - sharedPreferences.getLong("current time", 1L) > 86400000) {
            isTimeUpdate = true;
        }

        return isTimeUpdate;
    }

    public void saveTime() {
        long time = System.currentTimeMillis();

        SharedPreferences sharedPreferences = context.getSharedPreferences("time shared preferences"
                , Context.MODE_PRIVATE);

        sharedPreferences.edit()
                .putLong("current time", time)
                .apply();
    }
}
