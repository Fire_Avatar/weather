package com.weather.UI;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.weather.R;
import com.weather.models.WeatherModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailedWeatherActivity extends AppCompatActivity {
    @BindView(R.id.adw_weather_icon)
    ImageView weatherIcon;
    @BindView(R.id.adw_date)
    TextView date;
    @BindView(R.id.adw_temperature)
    TextView temperature;
    @BindView(R.id.adw_weather)
    TextView weather;
    @BindView(R.id.adw_feels_like)
    TextView feelsLike;
    @BindView(R.id.adw_sunrise)
    TextView sunrise;
    @BindView(R.id.adw_sunset)
    TextView sunset;
    @BindView(R.id.adw_humidity)
    TextView humidity;
    @BindView(R.id.adw_visibility)
    TextView visibility;
    @BindView(R.id.adw_cloudcover)
    TextView cloudcover;
    @BindView(R.id.adw_pressure)
    TextView pressure;
    @BindView(R.id.adw_wind_speed)
    TextView windSpeed;


    private WeatherModel weatherModel;
    private int dayNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_weather);
        ButterKnife.bind(this);

        weatherModel = (WeatherModel) getIntent().getSerializableExtra(MainActivity.WEATHER_MODEL_KEY);
        dayNumber = (int) getIntent().getSerializableExtra(MainActivity.DAY_NUMBER);

        setWeather();
    }

    private void setWeather() {
        Picasso.with(this)
                .load(weatherModel.data.weather.get(dayNumber).hourly.get(0).weatherIconUrl.get(0).value)
                .into(weatherIcon);

        date.setText(String.format("Date: %s", weatherModel.data.weather.get(dayNumber).date));
        temperature.setText(String.format("Temperature: %sC°", weatherModel.data.weather.get(dayNumber).hourly.get(0).tempC));
        weather.setText("Weather: ");

        for (int i = 0; i < weatherModel.data.weather.get(dayNumber).hourly.get(0).weatherDesc.size(); i++) {

            if (i != weatherModel.data.weather.get(dayNumber).hourly.get(0).weatherDesc.size() - 1) {
                String text = weather.getText().toString();
                weather.setText(text + weatherModel.data.weather.get(dayNumber).hourly.get(0).weatherDesc.get(i).value + ", ");
            } else {
                String text = weather.getText().toString();
                weather.setText(String.format("%s%s", text, weatherModel.data.weather.get(dayNumber).hourly.get(0).weatherDesc.get(i).value));

            }
        }

        feelsLike.setText("Feels lLike: " + weatherModel.data.weather.get(dayNumber).hourly.get(0).feelsLikeC + "C°");
        sunrise.setText(String.format("Sunrise: %s", weatherModel.data.weather.get(dayNumber).astronomy.get(0).sunrise));
        sunset.setText(String.format("Sunset: %s", weatherModel.data.weather.get(dayNumber).astronomy.get(0).sunset));
        humidity.setText("Humidity: " + weatherModel.data.weather.get(dayNumber).hourly.get(0).humidity + "%");
        visibility.setText("Visibility: " + weatherModel.data.weather.get(dayNumber).hourly.get(0).visibility + "km");
        cloudcover.setText("Cloudcover: " + weatherModel.data.weather.get(dayNumber).hourly.get(0).cloudCover + "%");
        pressure.setText(String.format("Pressure: %s", weatherModel.data.weather.get(dayNumber).hourly.get(0).pressure));
        windSpeed.setText("WindSpeed: " + weatherModel.data.weather.get(dayNumber).hourly.get(0).windSpeedKmph + "km/h");
    }
}
