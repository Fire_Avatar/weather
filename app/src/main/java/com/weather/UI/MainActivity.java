package com.weather.UI;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.weather.R;
import com.weather.adapters.ExampleRVAdapter;
import com.weather.database.DataKeeper;
import com.weather.models.WeatherModel;
import com.weather.net.QueryManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {
    public static final String WEATHER_MODEL_KEY = "weather key";
    public static final String DAY_NUMBER = "day number";

    protected RecyclerView.LayoutManager mLayoutManager;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    private ExampleRVAdapter mExampleRVAdapter;
    private WeatherModel mWeatherModel;
    private DataKeeper dataKeeper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        dataKeeper = new DataKeeper(this);

        if (!dataKeeper.isWeatherDataExist()) {
            if (dataKeeper.isTimeToUpdate()) {
                mProgressBar.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
                QueryManager.getWeather(new Callback<WeatherModel>() {
                    @Override
                    public void success(WeatherModel weatherModel, Response response) {
                        mWeatherModel = weatherModel;

                        mExampleRVAdapter = new ExampleRVAdapter(mWeatherModel.data.weather, MainActivity.this);
                        mExampleRVAdapter.setOnItemClickListener(new ExampleRVAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(WeatherModel item, int position) {
                                startActivity(new Intent(MainActivity.this, DetailedWeatherActivity.class)
                                        .putExtra(WEATHER_MODEL_KEY, mWeatherModel)
                                        .putExtra(DAY_NUMBER, position));
                            }
                        });

                        mRecyclerView.setAdapter(mExampleRVAdapter);

                        dataKeeper.saveWeather(mWeatherModel);
                        mProgressBar.setVisibility(View.GONE);
                        mRecyclerView.setVisibility(View.VISIBLE);

                        dataKeeper.saveTime();
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
            }
        } else {
            mWeatherModel = dataKeeper.getWeather();

            mExampleRVAdapter = new ExampleRVAdapter(mWeatherModel.data.weather, MainActivity.this);
            mExampleRVAdapter.setOnItemClickListener(new ExampleRVAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(WeatherModel item, int position) {
                    startActivity(new Intent(MainActivity.this, DetailedWeatherActivity.class)
                            .putExtra(WEATHER_MODEL_KEY, mWeatherModel)
                            .putExtra(DAY_NUMBER, position));
                }
            });

            mRecyclerView.setAdapter(mExampleRVAdapter);
        }
    }
}
